-- Inf2D Assignment 1

-- Good Scholarly Practice
-- Please remember the University requirement as regards all assessed work for credit.
-- Details about this can be found at:
-- http://www.ed.ac.uk/academic-services/students/undergraduate/discipline/academic-misconduct
-- and at:
-- http://web.inf.ed.ac.uk/infweb/admin/policies/academic-misconduct
-- Furthermore, you are required to take reasonable measures to protect your assessed work from
-- unauthorised access.For example, if you put any such work on a public repository then you must
-- set access permissions appropriately (generally permitting access only to yourself, or your
-- group in the case of group practicals).

module Inf2d where
import Data.List
import Debug.Trace
import Data.Ord
import Data.Maybe
-- Type synonyms for the data structures
-- Symbols are integers (negative integers represent negated symbols)
type Symbol = String
-- Clause = a disjuntion of symbols
type Clause = [Symbol]
-- Sentence = Statements. This is a list of a list of symbols
type Sentence = [[Symbol]]
-- Models are represented as a list of (Symbol,Boolean) tuples
type Model = [(Symbol, Bool)]
-- The knowledge base is represented as a list of statements
type KB = [Sentence]


-----------------------------------------------
-- STUDENT MATRICULATION NUMBER:
-----------------------------------------------
studentId :: String
studentId = "s1321070"

-------------------------------------------------
-- ASSIGNMENT TASKS
-- Refer to assignment sheet for details of tasks
--------------------------------------------------

----------TASK 1: REPRESENTATION (2 marks)----------------------------------------------------------
wumpusFact :: Sentence
wumpusFact = [["-B11", "P11", "P22", "P31"], ["-P11", "B11"]
              , ["-P22", "B11"], ["-P31", "B11"]]

----------TASK 2: GENERAL HELPER FUNCTIONS (10 marks)-----------------------------------------------

-- Finds the assigned literal to a symbol from a given model
lookupAssignment :: Symbol -> Model -> Maybe Bool
lookupAssignment = lookup

-- Negate a symbol
negateSymbol :: Symbol -> Symbol
negateSymbol symbol
  | isPrefixOf "-" symbol = drop 1 symbol
  | otherwise             = '-':symbol

-- Auxiliary function to negate the value whilst keeping the type
negateMaybeBool :: Maybe Bool -> Maybe Bool
negateMaybeBool = fmap not

-- For a given symbol, this function checks if it is negated(i.e., has a negation sign).
isNegated :: Symbol -> Bool
isNegated = isPrefixOf "-"

-- This function takes a symbol and returns an Symbol without any negation sign if the original
-- symbol had one.
getUnsignedSymbol :: Symbol -> Symbol
getUnsignedSymbol symbol
  | isNegated symbol = negateSymbol symbol
  | otherwise        = symbol

-- Gets a list of all symbols in for all given sentences
getSymbols :: [Sentence] -> [Symbol]
getSymbols stmts = nub $ map getUnsignedSymbol $ (concat . concat) stmts

----------TASK 3: TRUTH TABLE ENUMERATION AND ENTAILMENT (40 marks)---------------------------------

-- Function takes as input a list of symbols, and returns a list of models (all possible assignment
-- of True or False to the symbols.)
generateModels :: [Symbol] -> [Model]
generateModels [] = [[]]
generateModels (x:xs) = [ (x, False):model | model <- generateModels xs ] ++
                        [ (x, True):model | model <- generateModels xs ]

-- This function evaluates the truth value of a propositional sentence using the symbols
-- assignments in the model.
pLogicEvaluate :: Sentence -> Model -> Bool
pLogicEvaluate stmt model = and $ evalClauses stmt model

-- Auxiliary method to evaluate clauses of a sentence
evalClauses :: Sentence -> Model -> [Bool]
evalClauses [] _ = []
evalClauses (clause:stmt) model = any (fromMaybe False) (map (\sym -> lookupAssignment' sym model) clause) : evalClauses stmt model

-- Auxiliry function to handle negative symbols and properly negate value
lookupAssignment' :: Symbol -> Model -> Maybe Bool
lookupAssignment' symbol model
  | isNegated symbol = negateMaybeBool $ lookupAssignment (getUnsignedSymbol symbol) model
  | otherwise = lookupAssignment symbol model


-- This function checks the truth value of list of a propositional sentence using the symbols
-- assignments in the model. It returns true only when all sentences in the list are true.
plTrue :: [Sentence]-> Model -> Bool
plTrue sentences model = and [pLogicEvaluate sent model | sent <- sentences]

-- This function takes as input a knowledgebase (i.e. a list of propositional sentences),
-- a query (i.e. a propositional sentence), and a list of symbols.
-- It recursively enumerates the models of the domain using its symbols to check if there
-- is a model that satisfies the knowledge base and the query. It returns a list of all such models.
ttCheckAll :: [Sentence] -> Sentence -> [Symbol] -> [Model]
ttCheckAll kb query symbols = ttCheckAll' kb query symbols []
  where
    ttCheckAll' :: [Sentence] -> Sentence -> [Symbol] -> Model -> [Model]
    ttCheckAll' kb query [] model = if plTrue kb model && pLogicEvaluate query model then [model] else []
    ttCheckAll' kb query (p:rest) model = ttCheckAll' kb query rest ((p, True):model) ++
        ttCheckAll' kb query rest ((p, False):model)

-- This function determines if a model satisfes both the knowledge base and the query, returning
-- true or false.
ttEntails :: [Sentence] -> Sentence -> Bool
ttEntails kb query = not $ null $ ttCheckAll kb query $ getSymbols kb ++ getSymbols [query]


-- This function determines if a model satisfes both the knowledge base and the query.
-- It returns a list of all models for which the knowledge base entails the query.
ttEntailsModels :: [Sentence] -> Sentence -> [Model]
ttEntailsModels kb query = ttCheckAll kb query $ getSymbols kb ++ getSymbols [query]


----------TASK 4: DPLL (43 marks)-------------------------------------------------------------------

-- The early termination function checks if a sentence is true or false even with a
-- partially completed model.
earlyTerminate :: Sentence -> Model -> Bool
earlyTerminate sentence model = or $ evalClauses sentence model

-- This function finds pure symbol, i.e, a symbol that always appears with the same "sign" in all
-- clauses.
-- It takes a list of symbols, a list of clauses and a model as inputs.
-- It returns Just a tuple of a symbol and the truth value to assign to that
-- symbol. If no pure symbol is found, it should return Nothing
findPureSymbol :: [Symbol] -> [Clause] -> Model -> Maybe (Symbol, Bool)
findPureSymbol symbols clauses model = undefined

-- This function finds a unit clause from a given list of clauses and a model of assignments.
-- It returns Just a tuple of a symbol and the truth value to assign to that symbol. If no unit
-- clause is found, it should return Nothing.
findUnitClause :: [Clause] -> Model -> Maybe (Symbol, Bool)
findUnitClause clauses model = undefined


-- This function check the satisfability of a sentence in propositional logic. It takes as input a
-- list of clauses in CNF, a list of symbols for the domain, and model.
-- It returns true if there is a model which satises the propositional sentence.
-- Otherwise it returns false.
dpll :: [Clause] -> [Symbol] -> Bool
dpll clauses symbols = undefined

-- This function serves as the entry point to the dpll function. It takes a list clauses in CNF as
-- input and returns true or false.
-- It uses the dpll function above to determine the satisability of its input sentence.
dpllSatisfiable :: [Clause] -> Bool
dpllSatisfiable clauses = undefined

----------TASK 5: EVALUATION (5 marks)--------------------------------------------------------------
-- EVALUATION
-- a knowledge base (i.e. a sentence in propositional
-- logic), and a query sentence. Both items should have their clauses in CNF representation
-- and should be assigned to the following respectively:
evalKB :: [Sentence]
evalKB = undefined

evalQuery :: Sentence
evalQuery = undefined


-- RUNTIMES
-- Enter the average runtimes of the ttEntails and dpllSatisable functions respectively
runtimeTtEntails :: Double
runtimeTtEntails = undefined

runtimeDpll :: Double
runtimeDpll = undefined
